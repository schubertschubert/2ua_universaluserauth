SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema login_api
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `login_api` DEFAULT CHARACTER SET utf8 ;
USE `login_api` ;

-- -----------------------------------------------------
-- Table `login_api`.`entities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `login_api`.`entities` (
  `entityID` INT(11) NOT NULL AUTO_INCREMENT,
  `entityName` VARCHAR(100) NULL DEFAULT NULL,
  `entityCoName` VARCHAR(100) NULL,
  `entityEIN` VARCHAR(14) NULL COMMENT 'EIN (Employer Identification Number) is the Brazilian CNPJ\'s equivalent.' /* comment truncated */ /*
The format mask needs to be provided by the programing language, in agree with the regional options of the software.
*/,
  `entityAddress` VARCHAR(100) NULL,
  `entityDistrict` VARCHAR(45) NULL,
  `entityCity` VARCHAR(45) NULL,
  `entityCountry` VARCHAR(45) NULL,
  `entityEmail` VARCHAR(45) NULL,
  `entityPhone` VARCHAR(15) NULL COMMENT 'The format mask needs to be provided by the programing language, in agree with the regional options of the software.',
  PRIMARY KEY (`entityID`),
  UNIQUE INDEX `entityFEIN_UNIQUE` (`entityEIN` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `login_api`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `login_api`.`users` (
  `userID` INT(11) NOT NULL AUTO_INCREMENT,
  `userName` VARCHAR(45) NOT NULL,
  `userLastname` VARCHAR(45) NOT NULL,
  `userSSN` VARCHAR(15) NULL COMMENT 'SSN (Social Sucurity Number) is the Brazilian CPF\'s equivalent.' /* comment truncated */ /*
The format mask needs to be provided by the programing language, in agree with the regional options of the software.
*/,
  `userLogin` VARCHAR(10) NOT NULL,
  `userPswd` VARCHAR(45) NOT NULL,
  `userToken` VARCHAR(99) NULL DEFAULT NULL,
  `userDatesign` DATETIME NULL DEFAULT NULL,
  `userRemoteaddr` VARCHAR(45) NULL DEFAULT NULL,
  `userImg` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE INDEX `userLogin_UNIQUE` (`userLogin` ASC),
  UNIQUE INDEX `userSSN_UNIQUE` (`userSSN` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `login_api`.`entity_users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `login_api`.`entity_users` (
  `entityUserID` INT(11) NOT NULL AUTO_INCREMENT,
  `entityID_fk` INT NOT NULL,
  `userID_fk` INT NOT NULL,
  `entities_entityID` INT(11) NOT NULL,
  `users_userID` INT(11) NOT NULL,
  PRIMARY KEY (`entityUserID`),
  INDEX `fk_entity_users_entities1_idx` (`entities_entityID` ASC),
  INDEX `fk_entity_users_users1_idx` (`users_userID` ASC),
  CONSTRAINT `fk_entity_users_entities1`
    FOREIGN KEY (`entities_entityID`)
    REFERENCES `login_api`.`entities` (`entityID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_entity_users_users1`
    FOREIGN KEY (`users_userID`)
    REFERENCES `login_api`.`users` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '		';


-- -----------------------------------------------------
-- Table `login_api`.`user_logs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `login_api`.`user_logs` (
  `userLogID` INT(11) NOT NULL AUTO_INCREMENT,
  `userID_fk` INT NOT NULL,
  `userLogCookie` VARCHAR(100) NULL,
  `userLogDateStart` DATETIME NULL,
  `userLogDateEnd` DATETIME NULL,
  `userLogRemoteAddr` VARCHAR(45) NULL,
  `userLogURIs` TEXT NULL,
  `user_logscol` VARCHAR(45) NULL,
  `users_userID` INT(11) NOT NULL,
  PRIMARY KEY (`userLogID`),
  INDEX `fk_user_logs_users1_idx` (`users_userID` ASC),
  CONSTRAINT `fk_user_logs_users1`
    FOREIGN KEY (`users_userID`)
    REFERENCES `login_api`.`users` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
